﻿using System.Collections.Generic;  // for Dictionary and List


public struct Pair<T>
{
    public T x;
    public T y;


    public T this[int key]
    {
        get
        {
            key %= 2;
            return key == 0 ? x : y;
        }
        set
        {
            key %= 2;
            if (key == 0)
            {
                x = value;
                return;
            }
            y = value;
        }
    }
}


public class NxMDiscreteGameOfCompleteInformation
{
    public Dictionary<Pair<string>, Pair<double>> utilityMap;
    // actions are simply strings, with an action-profile (i, j) being just a pair of strings

    Pair<string[]>? __cache_actions = null;


    public Pair<string[]> Actions
    {
        get{
            if (__cache_actions.HasValue)
            {
                return __cache_actions.Value;
            }
            RefreshActions();
            return __cache_actions.Value;
        }
    }


    public void RefreshActions()
    {
        __cache_actions = null;
        List<string> _Actions1 = new List<string>();
        List<string> _Actions2 = new List<string>();
        foreach (KeyValuePair<Pair<string>, Pair<double>> p in utilityMap)
        {
            if (!_Actions1.Contains(p.Key[0]))
                _Actions1.Add(p.Key[0]);
            if (!_Actions2.Contains(p.Key[1]))
                _Actions2.Add(p.Key[1]);
        }
        string[] A1 = _Actions1.ToArray();
        string[] A2 = _Actions2.ToArray();
        __cache_actions = new Pair<string[]> { x = A1, y = A2 };
    }

    public string[] Actions1
    {
        get { return Actions.x; }
    }

    public string[] Actions2
    {
        get { return Actions.y; }
    }

    public int N
    {
        get
        {
            return Actions1.Length;
        }
    }

    public int M
    {
        get
        {
            return Actions2.Length;
        }
    }

    Pair<string>? __cache_gameSolution = null;

    public void RefreshGameUtilityMap(){
        RefreshActions();
        RefreshGameSolution();
    }

    public void RefreshGameSolution(){
        string[] A1 = Actions1;
        string[] A2 = Actions2;
        Dictionary<string, string> PlayerTwosBestResponse = new Dictionary<string, string>();
        foreach (string a1 in A1)
        {
            string br = A2[0];
            double v = utilityMap[new Pair<string> { x = a1, y = A2[0] }].y;
            for (int j = 1; j < A2.Length; j++)
            {
                double _v = utilityMap[new Pair<string> { x = a1, y = A2[j] }].y;
                if (_v > v) { v = _v; br = A2[j]; }
            }
            PlayerTwosBestResponse.Add(a1, br);
        }
        string a = A1[0];
        double u = utilityMap[new Pair<string> { x = a, y = PlayerTwosBestResponse[a] }].x;
        for (int i = 1; i < A1.Length; i++)
        {
            double _u = utilityMap[new Pair<string> { x = A1[i], y = PlayerTwosBestResponse[A1[i]] }].x;
            if (_u > u) { u = _u; a = A1[i]; }
        }
        string b = PlayerTwosBestResponse[a];
        __cache_gameSolution = new Pair<string> { x = a, y = b };
    }

    public Pair<string> SolveGame
    {
        get
        {
            if (__cache_gameSolution.HasValue)
            {
                return __cache_gameSolution.Value;
            }
            RefreshGameSolution();
            return __cache_gameSolution.Value;
        }
    }


    public NxMDiscreteGameOfCompleteInformation(Dictionary<Pair<string>, Pair<double>> _utilityMap)
    {
        utilityMap = _utilityMap;
    }

    public NxMDiscreteGameOfCompleteInformation(Dictionary<Pair<ushort>, Pair<double>> _utilityMap)
    {
        utilityMap = new Dictionary<Pair<string>, Pair<double>>();
        foreach (KeyValuePair<Pair<ushort>, Pair<double>> p in _utilityMap)
        {
           utilityMap.Add(
              new Pair<string> {x = p.Key.x.ToString(), y = p.Key.y.ToString()},
              new Pair<double> {x = p.Value.x,          y = p.Value.y}
           );
        }
    }


}



public class CustomTwoStep_St_NE_Game
{
    //   ** Actions I = {'0','1','2'} and J = {'0','1','2'}.
    //       st I = {'0','1'} U {'2'} = I1 U I2   // the "committal" decision is the last one
    //   ** Player 1 (row-chooser) first chooses between I1 and I2, publically, as a leader.
    //       ie T1 = Indicator[Player 1 chooses I1]
    //           -> Only the boolean T1 is chosen (not the way I is divided)
    //   ** THEN, we have a simutaneous game of complete info with actions {T1*I1 + (1-T1)*I2} VS J.


    public double[,] UtilityMatrix1 = new double[3, 3];
    public double[,] UtilityMatrix2 = new double[3, 3];

    Pair<string> SolveI1
    {
        get{
            Dictionary<Pair<ushort>, Pair<double>> dict = new Dictionary<Pair<ushort>, Pair<double>>();
            foreach (ushort a in new ushort[]{0, 1}){
                foreach (ushort b in new ushort[] {0, 1, 2}){
                    dict.Add(new Pair<ushort> { x = a, y = b },
                             new Pair<double> { x = UtilityMatrix1[a, b], y = UtilityMatrix2[a, b] }
                            );
                }
            }
            NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(dict);
            return game.SolveGame;
        }
    }


    Pair<string> SolveI2{
        get{
            Dictionary<Pair<ushort>, Pair<double>> dict = new Dictionary<Pair<ushort>, Pair<double>>();
            foreach (ushort a in new ushort[] { 2 }){
                foreach (ushort b in new ushort[] { 0, 1, 2 }){
                    dict.Add(new Pair<ushort> { x = a, y = b },
                             new Pair<double> { x = UtilityMatrix1[a, b], y = UtilityMatrix2[a, b] }
                            );
                }
            }
            NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(dict);
            return game.SolveGame;
        }
    }

    public Pair<string> SolveGame{
        get{
            Pair<string> G1Solution = SolveI1;
            Pair<string> G2Solution = SolveI2;
            double u1 = UtilityMatrix1[int.Parse(G1Solution.x), int.Parse(G1Solution.y)];
            double u2 = UtilityMatrix1[int.Parse(G2Solution.x), int.Parse(G2Solution.y)];
            return u1 > u2 ? G1Solution : G2Solution;
        }
    }
}




public class CustomThreeStep_St_NE_Game
{
    //   ** Actions I = {'0','1','2'} and J = {'0','1','2'}.
    //       st I = {'0','1'} U {'2'} = I1 U I2,
    //       and J = {'0','1'} U {'2'} = J1 U J2.
    //   ** Player 1 (row-chooser) first chooses between i=I1 and i=I2, publically, as a leader.
    //   ** Then we have Player 2 choosing, publically, between j=J1 and j=J2.
    //   ** THEN, we have a simutaneous game of complete info with actions i vs j.

    public double[,] UtilityMatrix1 = new double[3, 3];
    public double[,] UtilityMatrix2 = new double[3, 3];


    Pair<string> SolveI1J1
    {
        get{
            Dictionary<Pair<ushort>, Pair<double>> dict = new Dictionary<Pair<ushort>, Pair<double>>();
            foreach (ushort a in new ushort[] {0, 1 })
            {
                foreach (ushort b in new ushort[] { 0, 1 })
                {
                    dict.Add(new Pair<ushort> { x = a, y = b },
                             new Pair<double> { x = UtilityMatrix1[a, b], y = UtilityMatrix2[a, b] }
                            );
                }
            }
            NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(dict);
            return game.SolveGame;
        }
    }
    Pair<string> SolveI1J2
    {
        get
        {
            Dictionary<Pair<ushort>, Pair<double>> dict = new Dictionary<Pair<ushort>, Pair<double>>();
            foreach (ushort a in new ushort[] { 0, 1 })
            {
                foreach (ushort b in new ushort[] { 2 })
                {
                    dict.Add(new Pair<ushort> { x = a, y = b },
                             new Pair<double> { x = UtilityMatrix1[a, b], y = UtilityMatrix2[a, b] }
                            );
                }
            }
            NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(dict);
            return game.SolveGame;
        }
    }
    Pair<string> SolveI2J1
    {
        get
        {
            Dictionary<Pair<ushort>, Pair<double>> dict = new Dictionary<Pair<ushort>, Pair<double>>();
            foreach (ushort a in new ushort[] { 2 })
            {
                foreach (ushort b in new ushort[] { 0, 1 })
                {
                    dict.Add(new Pair<ushort> { x = a, y = b },
                             new Pair<double> { x = UtilityMatrix1[a, b], y = UtilityMatrix2[a, b] }
                            );
                }
            }
            NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(dict);
            return game.SolveGame;
        }
    }
    Pair<string> SolveI2J2
    {
        get
        {
            Dictionary<Pair<ushort>, Pair<double>> dict = new Dictionary<Pair<ushort>, Pair<double>>();
            foreach (ushort a in new ushort[] { 2 })
            {
                foreach (ushort b in new ushort[] { 2 })
                {
                    dict.Add(new Pair<ushort> { x = a, y = b },
                             new Pair<double> { x = UtilityMatrix1[a, b], y = UtilityMatrix2[a, b] }
                            );
                }
            }
            NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(dict);
            return game.SolveGame;
        }
    }


    public Pair<string> SolveGame
    {
        get
        {
            // if i = I1, what will j be? 
            Pair<string> GI1J1_solution = SolveI1J1;
            Pair<string> GI1J2_solution = SolveI1J2;
            double u11 = UtilityMatrix2[int.Parse(GI1J1_solution.x), int.Parse(GI1J1_solution.y)];
            double u12 = UtilityMatrix2[int.Parse(GI1J2_solution.x), int.Parse(GI1J2_solution.y)];
            Pair<string> G1_sol = u11 > u12 ? GI1J1_solution : GI1J2_solution;


            // if i = I2, what will j be? 
            Pair<string> GI2J1_solution = SolveI2J1;
            Pair<string> GI2J2_solution = SolveI2J2;
            double u21 = UtilityMatrix2[int.Parse(GI2J1_solution.x), int.Parse(GI2J1_solution.y)];
            double u22 = UtilityMatrix2[int.Parse(GI2J2_solution.x), int.Parse(GI2J2_solution.y)];
            Pair<string> G2_sol = u21 > u22 ? GI2J1_solution : GI2J2_solution;

            // does player 1 do better from i = I1 or from i = I2?
            double u1 = UtilityMatrix1[int.Parse(G1_sol.x), int.Parse(G1_sol.y)];
            double u2 = UtilityMatrix1[int.Parse(G2_sol.x), int.Parse(G2_sol.y)];
            return u1 > u2 ? G1_sol : G2_sol;
        }
    }
}
