﻿using System;

using FileStream = System.IO.FileStream;
using FileMode = System.IO.FileMode;
using FileAccess = System.IO.FileAccess;

using StreamWriter = System.IO.StringWriter;
using StreamReader = System.IO.StreamReader;


namespace MyFileManager
{
    public class FileHandler
    {

        public string fileName;


        public static string RESULTS_DIR =
            "/Users/PBVA007/Dropbox/LFG17_F18/Numerical Work/RansomwareInsurance/Results/";

        StreamReader reader;

        public StreamReader Reader
        {
            get { return reader;}
        }


        System.Text.StringBuilder writer;

        public System.Text.StringBuilder Writer
        {
            get { return writer; }
        }


        public FileHandler(string _fileName)
        {
            reader = new StreamReader(_fileName, System.Text.Encoding.UTF8);
        }
    }
}
