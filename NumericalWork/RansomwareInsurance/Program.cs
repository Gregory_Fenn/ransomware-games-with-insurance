﻿using System.Collections.Generic;  // for dictionary
using Console = System.Console;
using Math = System.Math;
using EnumUtils = System.Enum;
using System.IO;


// General solution idea: there are two possibilities for b_i and Z_i, giving 
// up to 16 feasible actions-choices for Firm1 and Firm2
// 16 is not a large number.
// The other actions in the game, (a, theta, r) are functions of (b_1, Z_1, b_2, Z_2)
// So we can solve a simple discrete 4 * 4 2-player finite game

// Stackleberg Leader Games correspond simply to crossing-out some (but not all) rows in advance


/*
First, select bIsLow vs bIsHigh, Zis0 vs Zis1  //high means guess output to alg
Then we can deduce the other actions
*/


/*
// the tuple struct is not used, but illustrates simple generic types T
// Obviously, arrays would work better.
public struct tuple<T>
{
    internal ushort _length;
    internal T[] _elements;

    public ushort Length
    {
        get
        {
            return _length;
        }
    }

    public ushort LastIndex
    {
        get
        {
            return (ushort)(_length - 1);
        }
    }

    public ushort FirstIndex
    {
        get
        {
            return (ushort)0;
        }
    }

    public tuple(T var0)
    {
        _length = 1;
        _elements = new T[1];
        _elements[0] = var0;
    }

    public tuple(T var0, T var1)
    {
        _length = 2;
        _elements = new T[2];
        _elements[0] = var0;
        _elements[1] = var1;
    }
    public tuple(T var0, T var1, T var2)
    {
        _length = 3;
        _elements = new T[3];
        _elements[0] = var0;
        _elements[1] = var1;
        _elements[2] = var2;
    }
    public tuple(T var0, T var1, T var2, T var3)
    {
        _length = 4;
        _elements = new T[4];
        _elements[0] = var0;
        _elements[1] = var1;
        _elements[2] = var2;
        _elements[3] = var3;
    }

    public T this[ushort index]
    {
        get
        {
            index %= _length;
            return _elements[index];
        }
        set
        {
            index %= _length;
            _elements[index] = value;
        }
    }
}
*/


public static class Util  // general maths and other pure functions
{
    static double G(double z, double E_i, double Beta_i, double F_i,
                            double C_A, double D, double L_i)
    {
        double A_i = Math.Sqrt(C_A * D / L_i);
        double B_i = 1 + Beta_i;
        double g = 2 * E_i;
        g *= Math.Pow(z, 2 * B_i + 2);
        g -= 2 * F_i * Math.Pow(z, 2 * Beta_i);
        g += A_i * z * (-L_i + 2 * B_i * Math.Pow(L_i, Beta_i));
        g -= 2 * B_i * Math.Pow(L_i, Beta_i);
        return g;
    }

    static double G_Dashed(double z, double E_i, double Beta_i, 
                                 double F_i, double C_A, double D, double L_i)
    {
        double A_i = Math.Sqrt(C_A * D / L_i);
        double B_i = 1 + Beta_i;
        double h = 4 * (B_i + 1) * E_i * Math.Pow(z, 2 * B_i + 1);
        h -= 4 * F_i * Beta_i * Math.Pow(z, 2 * Beta_i - 1);
        h += A_i * (-L_i + 2 * B_i * Math.Pow(L_i, Beta_i));
        return h;
    }

    public const double SEED = 0.000001;
    public const int ITERATIONS = 100;

    public static double SolveBHigh(double E_i, double Beta_i, double F_i,
                                    double C_A, double D, double L_i)
    {
        double x = SEED;

        for (int i = 0; i < ITERATIONS; i++)
        {
            x = x - (  G(x, E_i, Beta_i, F_i, C_A, D, L_i) 
                     / G_Dashed(x, E_i, Beta_i, F_i, C_A, D, L_i)  );
        }

        return x * x;
    }
}


public enum IndependentVariable
{
    C_D, D, C_A,  // I don't care about these much
    PI, DELTA,
    E1, F1, L1, BETA1,
    E2, F2, L2, BETA2
}



public enum InsuranceOrdering  // K = b1, b2, Z1, Z2 be default. {b1, b2} is always last and simultaneuos
{
    NE, Z1K, Z1Z2K
}


/*  // human-reference only, don't rely on computation with this
public enum FirmAction
{// j=0 [Z_2:F, b_2:low]  j=1 [Z_2:F, b_2:high]   j=2 [Z_2:T,  b_2:low]  
       i0j0,                       i0j1,           i0j2,     // i=0 => Z_1:false, b_1:low
       i1j0,                       i1j1,           i1j2,     // i=1 => Z_1:false, b_1:high
       i2j0,                       i2j1,           i2j2      // i=2 => Z_1:true,  b_1:low
}
*/



public static class Settings
{
    // For creating a big list of ResultsFiles, we just run the program over
    // and over again using new default settings here. 
    // To open, I can run "$open ressults_* -a Numbers" to open them all up
    // Good (ie interesing) combinations are given in columns as comments

    public const double default_C_D = 0.5;       //  0.5
    public const double default_D = 0.00000001;  //  0.00000001
    public const double default_C_A = 1;         //  1

    public const double default_PI = 0.015;      //  0.015
    public const double default_DELTA = 0.5;     // 0.5

    public const double default_E1 = 0.001;      // 0.001
    public const double default_F1 = 0.5;        // 0.5
    public const double default_L1 = 30;         // 20  // 25  //100
    public const double default_Beta1 = 2;     // 1  //10 //100  //5

    public const double default_E2 = 0.001;      // 0.001
    public const double default_F2 = 1;          // 0.5     // 1
    public const double default_L2 = 30;         // 20  //25    // 30 //100
    public const double default_Beta2 = 2;    // 1  //10 //100 //1
  


    public static bool CheckEmpiricalAssumptions
    {
        get
        {
            bool ok = true;
            ok = ok && (default_C_A * default_D < 1); 
            ok = ok && (default_C_A * default_D < default_E1);
            ok = ok && (default_E1 < default_F1);
            ok = ok && (default_F1 < default_L1);
            ok = ok && (default_C_A * default_D < default_E2);
            ok = ok && (default_E2 < default_F2);
            ok = ok && (default_F2 < default_L2);
            return ok;
        }
    }


    public static Dictionary<IndependentVariable, double>
        Dict_Defaults = new Dictionary<IndependentVariable, double>()
        {
            {IndependentVariable.C_D, default_C_D},
            {IndependentVariable.D, default_D},
            {IndependentVariable.C_A, default_C_A},

            {IndependentVariable.PI, default_PI},
            {IndependentVariable.DELTA, default_DELTA},

            {IndependentVariable.E1, default_E1},
            {IndependentVariable.F1, default_F1},
            {IndependentVariable.L1, default_L1},
            {IndependentVariable.BETA1, default_Beta1},

            {IndependentVariable.E2, default_E2},
            {IndependentVariable.F2, default_F2},
            {IndependentVariable.L2, default_L2},
            {IndependentVariable.BETA2, default_Beta2}
        };


    public static readonly double[] IV_Defaults =
    {
        default_C_D, default_D,  default_C_A, default_PI,    default_DELTA,
        default_E1,  default_F1, default_L1,  default_Beta1,
        default_E2,  default_F2, default_L2,  default_Beta2
    };


    public static readonly string[] IV_names =
    {
        "C_D",        "D",       "C_A",        "PI",    "DELTA",
        "E1",         "F1",      "L1",         "BETA1",
        "E2",         "F2",      "L2",         "BETA2"
    };


    public static readonly 
        Dictionary<IndependentVariable, double[]> Multipliers 
            = new Dictionary<IndependentVariable, double[]>()
    {
        {IndependentVariable.C_D, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },



        {IndependentVariable.D, new double[]  // fix to 0.00001 ish
            {

            }
        },



        {IndependentVariable.C_A, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },


        {IndependentVariable.PI, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },



        {IndependentVariable.DELTA, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },


        {IndependentVariable.E1, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },



        {IndependentVariable.F1, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },



        {IndependentVariable.L1, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },


        {IndependentVariable.BETA1, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },



        {IndependentVariable.E2, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },


        {IndependentVariable.F2, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },


        {IndependentVariable.L2, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        },


        {IndependentVariable.BETA2, new double[]
            {
                0.05, 0.075,
                0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
                0.6, 0.7, 0.8, 0.9, 0.92, 0.94, 0.96, 0.98, 1,
                1.02, 1.04, 1.06, 1.08,
                1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2,
                2.2, 2.4, 2.6, 2.8, 3
            }
        }
    };
}



public struct UtilityPair
{
    public double u;
    public double v;
}




public class Experiment
{
    public string IV_name;
    public IndependentVariable independentVariable;

    Dictionary<IndependentVariable, double> _GameParams = new Dictionary<IndependentVariable, double>();

    string _writeData;

    //Constructor
    public Experiment(IndependentVariable iv)
    {
        independentVariable = iv;
        _GameParams = new Dictionary<IndependentVariable, double>();
        _GameParams.Clear();
        _GameParams.Add(IndependentVariable.C_D, Settings.default_C_D);
        _GameParams.Add(IndependentVariable.D, Settings.default_D);
        _GameParams.Add(IndependentVariable.C_A, Settings.default_C_A);
        _GameParams.Add(IndependentVariable.PI, Settings.default_PI);
        _GameParams.Add(IndependentVariable.DELTA, Settings.default_DELTA);
        _GameParams.Add(IndependentVariable.E1, Settings.default_E1);
        _GameParams.Add(IndependentVariable.F1, Settings.default_F1);
        _GameParams.Add(IndependentVariable.L1, Settings.default_L1);
        _GameParams.Add(IndependentVariable.BETA1, Settings.default_Beta1);
        _GameParams.Add(IndependentVariable.E2, Settings.default_E2);
        _GameParams.Add(IndependentVariable.F2, Settings.default_F2);
        _GameParams.Add(IndependentVariable.L2, Settings.default_L2);
        _GameParams.Add(IndependentVariable.BETA2, Settings.default_Beta2);
        IV_name = Settings.IV_names[(int)iv];
    }


    Pair<string> _SolveGame(InsuranceOrdering ordering, out UtilityPair utilityPair)
    {
#region init
        double C_D = _GameParams[IndependentVariable.C_D];
        double D = _GameParams[IndependentVariable.D];
        double C_A = _GameParams[IndependentVariable.C_A];
        double PI = _GameParams[IndependentVariable.PI];
        double DELTA = _GameParams[IndependentVariable.DELTA];


        double E1 = _GameParams[IndependentVariable.E1];
        double F1 = _GameParams[IndependentVariable.F1];
        double L1 = _GameParams[IndependentVariable.L1];
        double BETA1 = _GameParams[IndependentVariable.BETA1];

        double E2 = _GameParams[IndependentVariable.E2];
        double F2 = _GameParams[IndependentVariable.F2];
        double L2 = _GameParams[IndependentVariable.L2];
        double BETA2 = _GameParams[IndependentVariable.BETA2];


        double b1_high = Util.SolveBHigh(E1, BETA1, F1, C_A, D, L1);
        double b2_high = Util.SolveBHigh(E2, BETA2, F2, C_A, D, L2);
        double b1_low = Math.Sqrt(F1 / E1);
        double b2_low = Math.Sqrt(F2 / E2);


        double b1 = 0;
        double b2 = 0;
        double Z1 = 0;
        double Z2 = 0;
#endregion

        /*
        Action_i = 0: Zi = 0; bi = b1_low
        Action_i = 1: Zi = 0; bi = b1_high
        Action_i = 2: Zi = 1; bi = b1_low
         */

        double[,] _utilityMatrix1 = new double[3, 3];
        double[,] _utilityMatrix2 = new double[3, 3];

        for (int i = 0; i < 9; i++)   // build the above two 3*3 matrices
        {
            switch (i)
            {
                case 0:  // 00
                    Z1 = 0; b1 = b1_low; Z2 = 0; b2 = b2_low;
                    break;
                case 1:  // 01
                    Z1 = 0; b1 = b1_low; Z2 = 0; b2 = b2_high;
                    break;
                case 2:  // 02
                    Z1 = 0; b1 = b1_low; Z2 = 1; b2 = b2_low;
                    break;


                case 3:  // 10
                    Z1 = 0; b1 = b1_high; Z2 = 0; b2 = b2_low;
                    break;
                case 4:  // 11
                    Z1 = 0; b1 = b1_high; Z2 = 0; b2 = b2_high;
                    break;
                case 5:  // 12
                    Z1 = 0; b1 = b1_high; Z2 = 1; b2 = b2_low;
                    break;


                case 6:  // 20
                    Z1 = 1; b1 = b1_low; Z2 = 0; b2 = b2_low;
                    break;
                case 7:  // 21
                    Z1 = 1; b1 = b1_low; Z2 = 0; b2 = b2_high;
                    break;
                case 8:  // 22
                    Z1 = 1; b1 = b1_low; Z2 = 1; b2 = b2_low;
                    break;


                default:
                    Console.Write("Error: no case found.");
                    utilityPair = new UtilityPair();
                    return new Pair<string>{x = "OH", y=  ".. dear"};
            }


            double R1 = (1 - Z1) * Math.Pow(L1 / b1, 1 + BETA1) +
                                (Z1 / (1 + _GameParams[IndependentVariable.DELTA])) * (L1 / b1);

            double R2 = (1 - Z2) * Math.Pow(L2 / b2, 1 + BETA2) +
                                (Z2 / (1 + _GameParams[IndependentVariable.DELTA])) * (L2 / b2);

            double r = Math.Max(R1, R2);
            bool theta = (R1 >= R2);
            double theta_1 = (theta) ? 1 : 0;
            double theta_2 = (theta) ? 0 : 1;
            double G = C_A * D + C_D + 2 * Math.Sqrt(C_D * C_A * D);
            double a = 0;
            if (r > G){a = Math.Sqrt((r * D) / C_A) - D;}

            int _col = i % 3;
            int _row = 2;
            if (i <= 5) { _row--; }
            if (i <= 2) { _row--; }
            //Hint to Undo: array value 3*col + row

            //So Firm1 chooses the row [slides the Y-axis of the 3x3 matrix], 
            //Firm2 chooses the column [slides the X-axis of the 3x3 matrix]

            double U1 = 
                E1 * b1 + (F1 / b1) + 
                (1 - Z1) * theta_1 * (a / (a + D)) * Math.Pow(L1 / b1, 1 + BETA1)
                + Z1 * _GameParams[IndependentVariable.PI] * L1;

            U1 *= -1;

            double U2 =
                E2 * b2   +   (F2 / b2) +
                (1 - Z2) * theta_2 * (a / (a + D)) * Math.Pow(L2 / b2, 1 + BETA2)
                + Z2 * _GameParams[IndependentVariable.PI] * L2;

            U2 *= -1;

            _utilityMatrix1[_row, _col] = U1;
            _utilityMatrix2[_row, _col] = U2;
        }


        Dictionary<Pair<ushort>, Pair<double>> _utilityMap = new Dictionary<Pair<ushort>, Pair<double>>();
        foreach (ushort i in new ushort[]{0,1,2})
        {
            foreach (ushort j in new ushort[] { 0, 1, 2 })
            {
                _utilityMap.Add(
                    new Pair<ushort> { x = i, y = j },
                    new Pair<double> { x = _utilityMatrix1[i, j], y = _utilityMatrix2[i, j] }
                );
            }
        }
        utilityPair = new UtilityPair();
        Pair<string> solve;
        int row, col;  // declare two ints to return as final solution

        switch (ordering)  // K = Kernal = {b1, b2, a1, a2, r, delta, pi} [simultaneous NE only]
        {
            case InsuranceOrdering.NE:
                NxMDiscreteGameOfCompleteInformation game = new NxMDiscreteGameOfCompleteInformation(_utilityMap);
                solve = game.SolveGame;
                row = int.Parse(solve.x);
                col = int.Parse(solve.y);
                utilityPair.u = _utilityMatrix1[row, col];
                utilityPair.v = _utilityMatrix2[row, col];
                return solve;

            case InsuranceOrdering.Z1K:
                // Firm1 can choose to force row 3 (i=2) only, or cross-out row 3 (i != 2)
                CustomTwoStep_St_NE_Game game2 = new CustomTwoStep_St_NE_Game
                {
                    UtilityMatrix1 = _utilityMatrix1, UtilityMatrix2 = _utilityMatrix2
                };
                solve = game2.SolveGame;
                row = int.Parse(solve.x);
                col = int.Parse(solve.y);
                utilityPair.u = _utilityMatrix1[row, col];
                utilityPair.v = _utilityMatrix2[row, col];
                return solve;

            case InsuranceOrdering.Z1Z2K:
                CustomThreeStep_St_NE_Game game3 = new CustomThreeStep_St_NE_Game
                {
                    UtilityMatrix1 = _utilityMatrix1,
                    UtilityMatrix2 = _utilityMatrix2
                };
                solve = game3.SolveGame;
                row = int.Parse(solve.x);
                col = int.Parse(solve.y);
                utilityPair.u = _utilityMatrix1[row, col];
                utilityPair.v = _utilityMatrix2[row, col];
                return solve;

            default:
                return new Pair<string> { x = "what.", y = "T.F." };
        }

    }


    public void Run(InsuranceOrdering ordering, StreamWriter resultsDatabase)
    {
        int experimentallength = Settings.Multipliers[independentVariable].Length;
        if (experimentallength > 0)
        {
            _writeData = string.Format("\n\n,{0}, {1}, Action1, Action2, U1, U2, U_mean\n", independentVariable.ToString(), ordering.ToString());
            resultsDatabase.WriteLine(_writeData);
        }

        for (int exp = 0; exp < experimentallength; exp++)
        {
            _GameParams[independentVariable] =
                Settings.Dict_Defaults[independentVariable] * Settings.Multipliers[independentVariable][exp];


            #region Check empirical assumption
            double __backup = Settings.Dict_Defaults[independentVariable];
            Settings.Dict_Defaults[independentVariable] = __backup * Settings.Multipliers[independentVariable][exp];
            bool check = Settings.CheckEmpiricalAssumptions;
            if (!check)
            {
                resultsDatabase.WriteLine(
                    string.Format(",{0},,{1},{2},{3},{4},{5},",
                        _GameParams[independentVariable],
                        double.NaN,
                        double.NaN,
                        double.NaN,
                        double.NaN,
                        double.NaN)
                    );
                Console.WriteLine("Empirical assumptions fail mid experiment. Break.");
                return;
            }
            Settings.Dict_Defaults[independentVariable] = __backup;
            #endregion


            Pair<string> solution = _SolveGame(ordering, out UtilityPair utilities);  // will be declared and assigned implicity

            resultsDatabase.WriteLine(
                string.Format(",{0},,{1},{2},{3},{4},{5},",
                              _GameParams[independentVariable],
                              solution.x,
                              solution.y,
                              utilities.u,
                              utilities.v,
                              (utilities.u + utilities.v) / 2
                             )
            );
        }
    }


    public void Run(StreamWriter resultsDatabase)
    {
        foreach (InsuranceOrdering ordering in EnumUtils.GetValues(typeof(InsuranceOrdering)))
        {
            _writeData = string.Format("\n\tORDER: {0}", ordering.ToString());
          
            resultsDatabase.AutoFlush = true;
            Run(ordering, resultsDatabase);
        }
    }
}



public class Program
{
    static string GetNextFileNumber()
    {
        const string filenamepath = "./../../Results/numbersused.counter";
        if (!File.Exists(filenamepath))
        {
            StreamWriter _writer = new StreamWriter(filenamepath);
            _writer.WriteLine("0");
            _writer.Close();
        }
        StreamReader reader = new StreamReader(filenamepath);
        int x = int.Parse(reader.ReadLine()) + 1;
        reader.Close();
        StreamWriter writer = new StreamWriter(filenamepath);
        writer.WriteLine(x.ToString());
        writer.Close();
        return x.ToString();
    }

    public static void Main() 
    {

        bool check = Settings.CheckEmpiricalAssumptions;
        if (!check){
            Console.WriteLine("Empirical assumptions fail. Abort.");
            return;
        }
        string x = GetNextFileNumber();

        Console.WriteLine("Note: actions are denoted by ints with the key:, "
                              + "Action_i = 0: Zi = 0; bi = b1_low, " +
                              "Action_i = 1: Zi = 0; bi = b1_high, " +
                              "Action_i = 2: Zi = 1; bi = b1_low\n");


        StreamWriter resultsDatabase = new StreamWriter("./../../Results/results_" + x + ".csv");

        string topLine = ",";
        string secondLine = ",";

        resultsDatabase.WriteLine(",Note: actions are denoted by key:, "
                              + "Action_i = 0: Zi = 0; bi = b1_low, " +
                              "Action_i = 1: Zi = 0; bi = b1_high, " +
                              "Action_i = 2: Zi = 1; bi = b1_low\n");

        resultsDatabase.Write(",Default Parameter Values:\n");


        foreach (IndependentVariable iv in EnumUtils.GetValues(typeof(IndependentVariable)))
        {
            double defaultValue = Settings.Dict_Defaults[iv];
            topLine += iv.ToString() + ",";
            secondLine += defaultValue.ToString() + ",";
        }


        resultsDatabase.WriteLine(topLine);
        resultsDatabase.WriteLine(secondLine);


        foreach (IndependentVariable iv in EnumUtils.GetValues(typeof(IndependentVariable)))
        {
            new Experiment(iv).Run(resultsDatabase);
        }

        resultsDatabase.Flush();
        resultsDatabase.Close();
    } 
}
