\contentsline {section}{\numberline {1}New Assumptions}{2}
\contentsline {section}{\numberline {2}Our Model}{3}
\contentsline {subsection}{\numberline {2.1}Firm risk aversion}{3}
\contentsline {subsection}{\numberline {2.2}The insurer}{5}
\contentsline {subsection}{\numberline {2.3}Modelling limitations}{5}
\contentsline {subsection}{\numberline {2.4}Firm Utility Functions}{6}
\contentsline {subsection}{\numberline {2.5}The Adversary's Utility}{8}
\contentsline {subsection}{\numberline {2.6}Game Ordering}{9}
\contentsline {section}{\numberline {3}The Equilibrium Strategy}{9}
\contentsline {subsection}{\numberline {3.1}\(p_i\) : do we pay the ransomware criminal?}{9}
\contentsline {subsection}{\numberline {3.2}How hard should we attack?}{10}
\contentsline {subsection}{\numberline {3.3}Simplifying the firm's utility function}{12}
\contentsline {subsection}{\numberline {3.4}Solving the attacker's strategy}{13}
\contentsline {section}{\numberline {4}Insurance behaviour in Nash Equilibrium}{14}
\contentsline {section}{\numberline {5}Player backup-strategies in a Nash Equilibrium}{15}
\contentsline {subsection}{\numberline {5.1}A plausible best-guess solution}{16}
\contentsline {subsection}{\numberline {5.2}Numerical Investigations in Nash Equilibria dynamics}{17}
\contentsline {section}{\numberline {6}Insurance behaviour in Stackleberg Equilibria, and other restrictions}{17}
\contentsline {section}{\numberline {7}Results}{19}
\contentsline {subsection}{\numberline {7.1}Question: is it ever rational to buy ransomware insurance, and does this depend the game parameters (other than \(\pi \), of course)?}{19}
\contentsline {subsection}{\numberline {7.2}Question: does public knowledge of player insurance behaviour improve or hinder social welfare? Or simply, does game order matter?}{20}
\contentsline {section}{\numberline {8}General Discussion and Practical Suggestions}{21}
\contentsline {section}{\numberline {9}Future Work}{21}
\contentsline {subsection}{\numberline {9.1}Insurer \(\mathcal {I}\)'s turn (as the first-move leader)}{21}
\contentsline {subsection}{\numberline {9.2}Discrimination in adversarial behaviour}{22}
\contentsline {subsection}{\numberline {9.3}Other potential directions}{22}
\contentsline {section}{\numberline {10}Conclusion}{22}
\contentsline {section}{Appendices}{22}
\contentsline {section}{\numberline {A}Remarks on the challenges of modelling \(\mathcal {I}\) as a rational agent}{22}
